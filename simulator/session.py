# SMART360 Simulator
# Copyright (C) 2023  Quentin Guimard
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# This file contains code under the following license:
#
# Copyright (c) 2020, UMass-LIDS
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
import argparse
from collections import namedtuple, deque
from copy import deepcopy
from fractions import Fraction
import sys

import numpy as np
import simplejson as json

from .br_adaptation import TrivialABR, MaxStallABR, BaselineABR, EnhancedPrefetchABR
from .buffer import TiledBuffer
from .headset import HeadsetModel
from ._logging import LogFile
from .network import NetworkModel
from .bw_estimation import EWMA
from .user import UserModel
from .vp_prediction import NoPredictor, StaticPredictor, DVMSPredictor


def str_to_class(classname):
    return getattr(sys.modules[__name__], classname)


def load_json(path):
    with open(path) as file:
        obj = json.load(file, parse_float=Fraction, parse_int=Fraction)
    return obj


ManifestInfo = namedtuple('ManifestInfo', 'segment_duration n_tiles tiles_x tiles_y bitrates segments')
NetworkPeriod = namedtuple('NetworkPeriod', 'time bandwidth latency')
CoordInformation = namedtuple('CoordInformation', 'play_time coord')


class SessionInfo:
    def __init__(self, manifest, buffer, B_max):
        self.manifest = manifest
        self.buffer = buffer
        self.B_max = B_max
        self.wall_time = 0

    def set_viewport_predictor(self, viewport_predictor):
        self.viewport_predictor = viewport_predictor

    def set_log_file(self, log_file):
        self.log_file = log_file

    def get_manifest(self):
        return self.manifest

    def get_buffer(self):
        return self.buffer

    def get_viewport_predictor(self):
        return self.viewport_predictor

    def get_log_file(self):
        return self.log_file

    def advance_wall_time(self, t):
        self.wall_time += t

    def get_wall_time(self):
        return self.wall_time


class Session:
    def __init__(self, config):
        self.config = config

        raw_manifest = load_json(config['manifest'])
        self.manifest = ManifestInfo(segment_duration=raw_manifest['segment_duration_ms'],
                                     n_tiles=raw_manifest['tiles_x'] * raw_manifest['tiles_y'],
                                     tiles_x=raw_manifest['tiles_x'],
                                     tiles_y=raw_manifest['tiles_y'],
                                     bitrates=raw_manifest['bitrates_kbps'],
                                     segments=raw_manifest['segment_sizes_bits'])
        del raw_manifest

        raw_network_trace = load_json(config['network_trace'])
        self.network_trace = [NetworkPeriod(time=p['duration_ms'],
                                            bandwidth=p['bandwidth_kbps'],
                                            latency=p['latency_ms'])
                              for p in raw_network_trace]
        del raw_network_trace

        raw_coord_trace = load_json(config['coord_trace'])
        self.coord_trace = [CoordInformation(play_time=p['time_ms'],
                                             coord=[float(x) for x in p['coordinates']])
                            for p in raw_coord_trace]
        del raw_coord_trace

        self.sizes_left = deepcopy(self.manifest.segments)

        self.B_max = Fraction(config['B_max'])
        self.delta_dl = Fraction(config['delta_dl'])

        self.buffer = TiledBuffer(self.B_max, self.manifest.segment_duration, self.manifest.n_tiles)
        self.session_info = SessionInfo(self.manifest, self.buffer, self.B_max)

        self.log_file = LogFile(self.session_info)
        self.session_info.set_log_file(self.log_file)
        self.log_file.log_any('config', config)

        self.user_model = UserModel(self.coord_trace)
        self.network_model = NetworkModel(self.network_trace)
        self.headset_model = HeadsetModel(self.session_info)

        self.estimator = EWMA(config)

        self.viewport_prediction = str_to_class(config['pred'])(self.session_info, self.headset_model, **config)

        self.session_info.set_viewport_predictor(self.viewport_prediction)

        self.ABR = str_to_class(config['ABR'])(config, self.session_info)

        self.did_startup = False

    def startup_stall_download(self, dl_schedule_list):
        dl_schedule_and_sizes = deque()
        downloaded_size = 0
        for schedule_element in dl_schedule_list:
            segment, tile, quality = schedule_element.values()
            self.buffer.put_in_buffer(segment, tile, quality)
            size = self.manifest.segments[segment][tile][quality]
            dl_schedule_and_sizes.appendleft({**schedule_element, 'size_left': size, 'total_size': size})
            downloaded_size += size
        latency, dl_time = self.network_model.download_group_of_tiles(dl_schedule_and_sizes)
        elapsed_time = latency + dl_time
        self.session_info.advance_wall_time(elapsed_time)
        self.estimator.push(latency, downloaded_size / dl_time)
        if self.did_startup:
            self.log_file.log_stall(elapsed_time, downloaded_size)
        else:
            self.log_file.log_startup(elapsed_time, downloaded_size)

    def play_and_download(self, dl_schedule_list, delta_dl):
        dl_schedule_and_sizes = deque()
        for schedule_element in dl_schedule_list:
            segment, tile, quality = schedule_element.values()
            try:
                size = self.manifest.segments[segment][tile][quality]
                size_left = self.sizes_left[segment][tile][quality]
                dl_schedule_and_sizes.appendleft({**schedule_element, 'size_left': size_left, 'total_size': size})
            except IndexError:
                print(f"Error accessing manifest for segment={segment}, tile={tile}, quality={quality}")
                print(f"Manifest segments shape: {np.array(self.manifest.segments).shape}")
                if segment < len(self.manifest.segments):
                    print(f"Available qualities for this segment and tile: {len(self.manifest.segments[segment][tile])}")
                else:
                    print(f"Segment {segment} is out of range")
                # Skip this item and continue with the next one
                continue
        delta_dl_left = delta_dl
        latency_left = self.network_model.request_group_of_tiles()
        self.log_file.log_time_latency(latency_left)
        self.estimator.push_latency(latency_left)
        while delta_dl_left > 0 and self.buffer.get_play_head() < len(self.manifest.segments) * self.manifest.segment_duration:
            update_coord = False
            time_until_coord = self.user_model.time_to_next_coord()
            time_until_segment = self.buffer.time_to_next_segment()
            time_to_next = min(delta_dl_left, time_until_coord, time_until_segment)
            if time_until_coord == time_to_next:
                update_coord = True
            coord = self.user_model.get_coord()
            self.log_file.log_coord(coord)
            visible_tiles = np.argwhere(np.array(self.headset_model.get_visible_tiles(coord))).flatten()
            tiles_in_buffer = np.array(self.buffer.get_buffer_head_content())
            visible_tiles_quality = tiles_in_buffer[visible_tiles]
            while np.any(visible_tiles_quality == -1):  # STALL
                blank_tiles = visible_tiles[np.argwhere(visible_tiles_quality == -1).flatten()]
                stall_dl_schedule_list = self.ABR.stall_dl_schedule(blank_tiles)
                self.log_file.log_dl_schedule(stall_dl_schedule_list)
                self.session_info.advance_wall_time(latency_left)
                latency_left = 0
                self.startup_stall_download(stall_dl_schedule_list)
                self.log_file.log_dl_success(stall_dl_schedule_list)
                stall_dl_schedule_set = {tuple(element.values())[:2] for element in stall_dl_schedule_list}
                dl_schedule_and_sizes_list = list(dl_schedule_and_sizes)[::-1]
                to_del = []
                for i, element in enumerate(dl_schedule_and_sizes_list):
                    if stall_dl_schedule_set:
                        segment, tile, *_ = element.values()
                        set_element = (segment, tile)
                        if set_element in stall_dl_schedule_set:
                            to_del.append(i)
                            stall_dl_schedule_set.remove(set_element)
                    else:
                        break
                for i in to_del[::-1]:
                    del dl_schedule_and_sizes_list[i]
                dl_schedule_and_sizes = deque(dl_schedule_and_sizes_list[::-1])
                tiles_in_buffer = np.array(self.buffer.get_buffer_head_content())
                visible_tiles_quality = tiles_in_buffer[visible_tiles]
            self.log_file.log_now_playing(time_to_next)
            self.log_file.log_visible_tiles(visible_tiles)
            self.log_file.log_buffer_head(tiles_in_buffer)
            self.log_file.log_visible_quality(visible_tiles_quality)
            if latency_left > 0 and time_to_next < latency_left:
                latency_left -= time_to_next
                delta_dl_left -= time_to_next
                self.session_info.advance_wall_time(time_to_next)
                self.user_model.play(time_to_next)
                self.buffer.play(time_to_next)
            else:
                time_to_next -= latency_left
                delta_dl_left -= latency_left
                self.session_info.advance_wall_time(latency_left)
                self.user_model.play(latency_left)
                self.buffer.play(latency_left)
                latency_left = 0
                if dl_schedule_and_sizes and time_to_next:
                    time_elapsed_dl, bits_downloaded, dl_success_list = self.network_model.download_group_of_tiles_time_constrained(dl_schedule_and_sizes, time_to_next)
                    time_to_next -= time_elapsed_dl
                    delta_dl_left -= time_elapsed_dl
                    self.session_info.advance_wall_time(time_elapsed_dl)
                    self.user_model.play(time_elapsed_dl)
                    self.buffer.play(time_elapsed_dl)
                    self.estimator.push_bandwidth(bits_downloaded / time_elapsed_dl)
                    self.log_file.log_time_downloading(time_elapsed_dl)
                    self.log_file.log_bits_downloaded(bits_downloaded)
                    self.log_file.log_dl_success(dl_success_list)
                    self.log_file.log_dl_schedule_left(dl_schedule_and_sizes)
                    for element in dl_success_list:
                        segment, tile, quality, size_left, total_size = element.values()
                        assert size_left == 0
                        self.buffer.put_in_buffer(segment, tile, quality)
            delta_dl_left -= time_to_next
            self.session_info.advance_wall_time(time_to_next)
            self.user_model.play(time_to_next)
            self.buffer.play(time_to_next)
            self.network_model.delay_network(time_to_next)
            if update_coord:
                self.viewport_prediction.update_coord(self.user_model.get_coord())
        return dl_schedule_and_sizes

    def run(self):
        video_time = self.manifest.segment_duration * len(self.manifest.segments)
        cycle_index = 0
        self.log_file.log_new_cycle(cycle_index)

        startup_dl_schedule = self.ABR.startup_dl_schedule()
        self.log_file.log_dl_schedule(startup_dl_schedule)
        self.startup_stall_download(startup_dl_schedule)
        self.log_file.log_dl_success(startup_dl_schedule)
        self.did_startup = True

        coord = self.user_model.get_coord()
        self.viewport_prediction.update_coord(coord)
        self.log_file.log_coord(coord)

        while self.session_info.buffer.get_play_head() < video_time:
            cycle_index += 1
            self.log_file.log_new_cycle(cycle_index)

            rtt = self.estimator.get_latency()
            tput = self.estimator.get_bandwidth()
            self.log_file.log_network_estimation(rtt, tput, cycle_index)
            time_before = self.session_info.get_wall_time()

            dl_schedule = self.ABR.decide_dl_schedule(rtt, tput, self.delta_dl)
            self.log_file.log_dl_schedule(dl_schedule)

            dl_schedule_left = self.play_and_download(dl_schedule, self.delta_dl)
            self.log_file.log_dl_schedule_left(dl_schedule_left)

            if dl_schedule_left:
                segment, tile, quality, size_left, _ = dl_schedule_left[-1].values()
                self.sizes_left[segment][tile][quality] = size_left

            time_after = self.session_info.get_wall_time()
            self.log_file.log_network_actual(*self.network_model._true_network_metrics(time_after - time_before), cycle_index)

            self.log_file.log_buffer(self.buffer)

    def end(self):
        self.log_file.dump(self.config['log_file'])

