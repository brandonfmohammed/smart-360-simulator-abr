# SMART360 Simulator
# Copyright (C) 2023  Quentin Guimard
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# This file contains code under the following license:
#
# Copyright (c) 2020, UMass-LIDS
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
import numpy as np


class TiledBuffer:
    def __init__(self, B_max, segment_duration, n_tiles):
        self.B_max = B_max
        self.segment_duration = segment_duration
        self.buffer = np.full((int(B_max), int(n_tiles)), -1, dtype=np.int8)
        self.played_segments = 0
        self.played_segment_partial = 0  # measured in ms

    def get_played_segments(self):
        return self.played_segments

    def get_played_segment_partial(self):
        return self.played_segment_partial

    def get_buffer_head_content(self):
        return self.buffer[0]

    def get_buffer_content(self):
        return self.buffer

    def get_play_head(self):
        return self.played_segments * self.segment_duration + self.played_segment_partial

    def put_in_buffer(self, segment_index, tile_index, quality):
        buf_seg_idx = int(segment_index - self.played_segments)
        assert buf_seg_idx < self.B_max
        if buf_seg_idx >= 0:
            self.buffer[buf_seg_idx, tile_index] = quality

    def time_to_next_segment(self):
        return self.segment_duration - self.played_segment_partial

    def play(self, time):
        assert time <= self.time_to_next_segment()
        self.played_segment_partial += time
        if self.played_segment_partial == self.segment_duration:
            self._next_segment()

    def _next_segment(self):
        self.buffer[:-1] = self.buffer[1:]
        self.buffer[-1] = -1
        self.played_segments += 1
        self.played_segment_partial = 0
