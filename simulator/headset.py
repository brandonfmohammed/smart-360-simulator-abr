# SMART360 Simulator
# Copyright (C) 2023  Quentin Guimard
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# This file contains code under the following license:
#
# Copyright (c) 2020, UMass-LIDS
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
import sys
import json

import numpy as np


class HeadsetModel:
    def __init__(self, session_info):
        manifest = session_info.get_manifest()
        self.session_info = session_info
        self.headset_config = HeadsetConfig('./config/headset_config.json', int(manifest.tiles_x), int(manifest.tiles_y))

        log_file = self.session_info.get_log_file()
        tile_sequence = self.headset_config.tile_sequence
        if len(tile_sequence) > 0:
            tx, ty = tile_sequence[0]
            log_file.log_any('first_tile', (tx, ty, self.describe_x_y(tx, ty)))
            if len(tile_sequence) > 2:
                txx, tyy = tile_sequence[1]
                log_file.log_any('second_tile', (txx, tyy, self.describe_x_y_direction(tx, ty, txx, tyy)))
            if len(tile_sequence) > 1:
                tx, ty = tile_sequence[-1]
                log_file.log_any('last_tile', (tx, ty, self.describe_x_y(tx, ty)))

    def describe_x_y(self, x, y):
        if y == 0:
            describe_y = 'top'
        elif y == self.headset_config.tiles_y - 1:
            describe_y = 'bottom'
        else:
            raise ValueError(f'Bad value for y: {y}')

        if x == 0:
            describe_x = 'left'
        elif x == self.headset_config.tiles_x - 1:
            describe_x = 'right'
        else:
            raise ValueError(f'Bad value for x: {x}')

        return '%s, %s' % (describe_y, describe_x)

    def describe_x_y_direction(self, x, y, xx, yy):
        assert (y == yy or x == xx)
        assert (y != yy or x != xx)

        if y == 0 and yy == y:
            describe_y = 'top'
        elif y == 0 and yy != y:
            describe_y = 'down'
        elif y == self.headset_config.tiles_y - 1 and yy == y:
            describe_y = 'bottom'
        elif y == self.headset_config.tiles_y - 1 and yy != y:
            describe_y = 'up'
        else:
            raise ValueError(f'Bad value for y: {y} and yy: {yy}')

        if x == 0 and xx == x:
            describe_x = 'left'
        elif x == 0 and xx != x:
            describe_x = 'rightward'
        elif x == self.headset_config.tiles_x - 1 and xx == x:
            describe_x = 'right'
        elif x == self.headset_config.tiles_x - 1 and xx != x:
            describe_x = 'leftward'
        else:
            raise ValueError(f'Bad value for x: {x} and xx: {xx}')

        return '%s, %s' % (describe_y, describe_x)

    def get_visible_tiles(self, coord):
        return self.headset_config.get_tiles(coord)


class HeadsetConfig:
    def __init__(self, config_file, tiles_x, tiles_y):
        with open(config_file) as file:
            obj = json.load(file)
            self.tiles_x = tiles_x
            self.tiles_y = tiles_y
            fov_x_degrees = int(obj['fov_x_degrees'])
            fov_y_degrees = int(obj['fov_y_degrees'])

            self.fov_x_norm = fov_x_degrees / 360
            self.fov_y_norm = fov_y_degrees / 180

            self.y_lines = np.linspace(0, 1, self.tiles_y + 1)
            self.dig_x_lines = np.linspace(0, 1, self.tiles_x + 1)[1:]
            self.dig_y_lines = np.linspace(0, 1, self.tiles_y + 1)[1:]

            if tiles_x < 1 or tiles_y < 1:
                print('Headset configuration "%s" has bad "tiles_x" or "tiles_y".' % config_file, file=sys.stderr)
                sys.exit(1)

            if tiles_x == 1 and tiles_y == 1:
                coord = [(0, 0)]
            else:
                x_begin = int(obj['tile_0']['x'])
                y_begin = int(obj['tile_0']['y'])
                x_step = int(obj['tile_1']['x']) - x_begin
                y_step = int(obj['tile_1']['y']) - y_begin

                if ((x_step == 0 and y_step == 0) or
                        (x_step != 0 and y_step != 0) or
                        (tiles_x == 1 and x_step != 0) or
                        (tiles_y == 1 and y_step != 0) or
                        (x_begin != 0 and x_begin != tiles_x - 1) or
                        (y_begin != 0 and y_begin != tiles_y - 1) or
                        (y_step == 0 and x_begin == 0 and x_step != 1) or
                        (y_step == 0 and x_begin != 0 and x_step != -1) or
                        (x_step == 0 and y_begin == 0 and y_step != 1) or
                        (x_step == 0 and y_begin != 0 and y_step != -1)):
                    print('Headset configuration "%s" has bad "tile_0" or "tile_1".' % config_file, file=sys.stderr)
                    sys.exit(1)

                if x_begin == 0:
                    xr = range(tiles_x)
                else:
                    xr = range(tiles_x - 1, -1, -1)

                if y_begin == 0:
                    yr = range(tiles_y)
                else:
                    yr = range(tiles_y - 1, -1, -1)

                coord = []
                if y_step == 0:
                    for y in yr:
                        for x in xr:
                            coord += [(x, y)]
                else:
                    for x in xr:
                        for y in yr:
                            coord += [(x, y)]

            self.tile_sequence = tuple(coord)

    def get_tiles(self, coordinates, projection_mode='realistic'):
        x, y, z = map(float, coordinates)

        r = np.sqrt(x * x + y * y + z * z)
        theta = np.remainder(np.arctan2(y, x), 2 * np.pi)
        phi = np.arccos(z / r)

        fov_center_x = theta / (2 * np.pi)
        fov_center_y = phi / np.pi

        fov_top_y = max(0.0, fov_center_y - self.fov_y_norm / 2)
        fov_bottom_y = min(1.0, fov_center_y + self.fov_y_norm / 2)

        tiles = np.zeros((self.tiles_y, self.tiles_x))

        if projection_mode == 'realistic':
            y_lines = self.y_lines[(self.y_lines > fov_top_y) & (self.y_lines < fov_bottom_y)]
            y_lines = np.array([fov_top_y] + y_lines.tolist() + [fov_bottom_y])
            n_lines = len(y_lines)
            for y_idx, y_line in enumerate(y_lines):
                if y_idx == 0:
                    tile_y_above = min(np.digitize(y_line, self.dig_y_lines), self.tiles_y - 1)
                else:
                    tile_y_above = max(np.digitize(y_line, self.dig_y_lines) - 1, 0)
                if y_idx == n_lines - 1:
                    tile_y_below = max(np.digitize(y_line, self.dig_y_lines) - 1, 0)
                else:
                    tile_y_below = min(np.digitize(y_line, self.dig_y_lines), self.tiles_y - 1)
                lat = (y_line - 0.5) * np.pi
                length = min(1.0, self.fov_x_norm / np.cos(lat))
                if length > 1.0 - 1 / self.tiles_x:
                    tiles[tile_y_above, :] = 1
                    tiles[tile_y_below, :] = 1
                    continue
                xmin = np.remainder(fov_center_x - length / 2, 1.0)
                xmax = np.remainder(fov_center_x + length / 2, 1.0)
                tile_xmin = np.digitize(xmin, self.dig_x_lines)
                tile_xmax = np.digitize(xmax, self.dig_x_lines)
                if xmin >= xmax:
                    tiles[tile_y_above, :tile_xmax + 1] = 1
                    tiles[tile_y_below, :tile_xmax + 1] = 1
                    tiles[tile_y_above, tile_xmin:] = 1
                    tiles[tile_y_below, tile_xmin:] = 1
                else:
                    tiles[tile_y_above, tile_xmin:tile_xmax + 1] = 1
                    tiles[tile_y_below, tile_xmin:tile_xmax + 1] = 1
        elif projection_mode == 'rectangle':
            fov_left_x = np.remainder(fov_center_x - self.fov_x_norm / 2, 1.0)
            fov_right_x = np.remainder(fov_center_x + self.fov_x_norm / 2, 1.0)
            tile_xmin = np.digitize(fov_left_x, np.linspace(0, 1, self.tiles_x + 1)[1:])
            tile_xmax = np.digitize(fov_right_x, np.linspace(0, 1, self.tiles_x + 1)[1:])
            tile_ymin = min(np.digitize(fov_top_y, np.linspace(0, 1, self.tiles_y + 1)[1:]), self.tiles_y - 1)
            tile_ymax = min(np.digitize(fov_bottom_y, np.linspace(0, 1, self.tiles_y + 1)[1:]), self.tiles_y - 1)
            if fov_left_x >= fov_right_x:
                tiles[tile_ymin:tile_ymax + 1, :tile_xmax + 1] = 1
                tiles[tile_ymin:tile_ymax + 1, tile_xmin:] = 1
            else:
                tiles[tile_ymin:tile_ymax + 1, tile_xmin:tile_xmax + 1] = 1
        else:
            raise ValueError(f'Unknown projection mode: {projection_mode}')

        return tiles.flatten().tolist()

    def expand_fov(self, tile_scores):
        tile_fov = np.array(tile_scores).reshape((self.tiles_y, self.tiles_x))
        k = 0
        while (tile_fov == 0.).any():
            curr_value = np.unique(tile_fov)[::-1][k]
            curr_val_idx = np.argwhere(tile_fov == curr_value)
            expand_idx = []
            for i, j in curr_val_idx:
                expand_idx.append((max(i - 1, 0), j))
                expand_idx.append((min(i + 1, self.tiles_y - 1), j))
                expand_idx.append((i, (j - 1) % self.tiles_x))
                expand_idx.append((i, (j + 1) % self.tiles_x))
                expand_idx.append((max(i - 1, 0), (j - 1) % self.tiles_x))
                expand_idx.append((min(i + 1, self.tiles_y - 1), (j - 1) % self.tiles_x))
                expand_idx.append((max(i - 1, 0), (j + 1) % self.tiles_x))
                expand_idx.append((min(i + 1, self.tiles_y - 1), (j + 1) % self.tiles_x))
            expand_idx = tuple(zip(*list(set(expand_idx))))
            tile_fov[expand_idx] = np.full_like(tile_fov[expand_idx], curr_value / 4).clip(tile_fov[expand_idx])
            k += 1
        return tile_fov.flatten()
