# SMART360 Simulator
# Copyright (C) 2023  Quentin Guimard
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import multiprocessing
import os

import numpy as np
import pandas as pd
from tqdm.auto import tqdm
import logging
from concurrent.futures import ThreadPoolExecutor, as_completed
from concurrent.futures import ProcessPoolExecutor, as_completed, TimeoutError


BASE_DIR = '/SMART360-simulator/'
SRC_DIR = BASE_DIR + 'output'
SRC_LIM = len(SRC_DIR) + 1
CONCATENATED_DIR = BASE_DIR + 'notebooks/concatenated_df'
METRICS_DIR = BASE_DIR + 'notebooks/metrics_df'
CONCATENATED_METRICS_DIR = BASE_DIR + 'notebooks/concatenated_metrics_df'


logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s:%(message)s')


def concatenate_dataframes(item):
    (v, bmin, df_name), files = item
    dataframes = []
    for file in files:
        try:
            ABR, pred, K, B_min, B_max, video, user, tiling, segment, network, delta_dl, r, EWMA_alpha, _ = file[SRC_LIM:].split('/')
            df = pd.read_feather(file)
            df['ABR'] = ABR.split('-')[0]
            df['pred'] = pred
            df['K'] = int(K.split('=')[1])
            df['B_min'] = int(B_min.split('=')[1])
            df['B_max'] = int(B_max.split('=')[1])
            df['video'] = video.split('-')[1]
            df['user'] = user.split('-')[1]
            df['tiling'] = tiling.split('-')[0]
            df['segment_size'] = int(segment.split('-')[0])
            df['network_trace'] = network.split('-')[1]
            df['delta_dl'] = int(delta_dl.split('=')[1])
            df['r'] = int(r.split('=')[1])
            df['EWMA_alpha'] = float(EWMA_alpha.split('=')[1])
            dataframes.append(df)
        except Exception as e:
            logging.error(f"Error processing file {file}: {e}")
    
    if dataframes:
        df = pd.concat(dataframes).reset_index(drop=True)
        save_path = f'{CONCATENATED_DIR}/{v}/{bmin}/{df_name}.feather'
        os.makedirs(os.path.dirname(save_path), exist_ok=True)
        df.to_feather(save_path)
        logging.info(f"Saved concatenated dataframe to {save_path}")

def concatenate_all_dataframes(n_cpus=max(1, os.cpu_count() - 2), task_timeout=300):
    files_dict = {}
    for ABR in sorted(os.listdir(SRC_DIR)):
        for pred in sorted(os.listdir(f'{SRC_DIR}/{ABR}')):
            for K in sorted(os.listdir(f'{SRC_DIR}/{ABR}/{pred}')):
                for B_min in sorted(os.listdir(f'{SRC_DIR}/{ABR}/{pred}/{K}/')):
                    bmin = B_min.split('=')[1]
                    for B_max in sorted(os.listdir(f'{SRC_DIR}/{ABR}/{pred}/{K}/{B_min}')):
                        for video in sorted(os.listdir(f'{SRC_DIR}/{ABR}/{pred}/{K}/{B_min}/{B_max}')):
                            v = video.split('-')[1]
                            for user in sorted(os.listdir(f'{SRC_DIR}/{ABR}/{pred}/{K}/{B_min}/{B_max}/{video}')):
                                for tiling in sorted(os.listdir(f'{SRC_DIR}/{ABR}/{pred}/{K}/{B_min}/{B_max}/{video}/{user}')):
                                    for segment in sorted(os.listdir(f'{SRC_DIR}/{ABR}/{pred}/{K}/{B_min}/{B_max}/{video}/{user}/{tiling}')):
                                        for network in sorted(os.listdir(f'{SRC_DIR}/{ABR}/{pred}/{K}/{B_min}/{B_max}/{video}/{user}/{tiling}/{segment}')):
                                            for delta_dl in sorted(os.listdir(f'{SRC_DIR}/{ABR}/{pred}/{K}/{B_min}/{B_max}/{video}/{user}/{tiling}/{segment}/{network}')):
                                                for r in sorted(os.listdir(f'{SRC_DIR}/{ABR}/{pred}/{K}/{B_min}/{B_max}/{video}/{user}/{tiling}/{segment}/{network}/{delta_dl}')):
                                                    for EWMA_alpha in sorted(os.listdir(f'{SRC_DIR}/{ABR}/{pred}/{K}/{B_min}/{B_max}/{video}/{user}/{tiling}/{segment}/{network}/{delta_dl}/{r}')):
                                                        for df_name in ['bw_util', 'downloaded', 'stall', 'startup']:
                                                            file_name = f'{SRC_DIR}/{ABR}/{pred}/{K}/{B_min}/{B_max}/{video}/{user}/{tiling}/{segment}/{network}/{delta_dl}/{r}/{EWMA_alpha}/logs-{df_name}.feather'
                                                            if os.path.exists(file_name):
                                                                try:
                                                                    files_dict[(v, bmin, df_name)].append(file_name)
                                                                except KeyError:
                                                                    files_dict[(v, bmin, df_name)] = [file_name]
                                                            else:
                                                                logging.warning(f'{file_name} does not exist!')

    logging.info("Starting multiprocessing pool for dataframe concatenation")
    
    with ProcessPoolExecutor(max_workers=n_cpus) as executor:
        futures = {executor.submit(concatenate_dataframes, item): item for item in files_dict.items()}
        with tqdm(total=len(files_dict)) as pbar:
            for future in as_completed(futures):
                item = futures[future]
                try:
                    future.result(timeout=task_timeout)
                except TimeoutError:
                    logging.error(f"Task {item} timed out.")
                except Exception as e:
                    logging.error(f"Task {item} generated an exception: {e}")
                pbar.update()

def process_metrics(video, B_min, abr_strategy):
    # Ensure the metrics directory exists
    metrics_path = f'{METRICS_DIR}/{video}/{B_min}/{abr_strategy}'
    #print(f"Creating directory: {metrics_path}")  # Debug statement

    os.makedirs(metrics_path, exist_ok=True)
    #print(f"Directory ensured: {metrics_path}")
   
    # Load the downloaded data which includes all ABR strategies
    downloaded_df = pd.read_feather(f'/{CONCATENATED_DIR}/{video}/{B_min}/downloaded.feather')

    # Filter for the specific ABR strategy
    strategy_df = downloaded_df[downloaded_df['ABR'] == abr_strategy].copy()

    # Compute the linear and logarithmic quality metrics for the filtered data
    try:
        strategy_df['q_lin'] = 2 ** strategy_df['quality']
        strategy_df['q_log'] = strategy_df['quality'] + 1
        strategy_df.drop(columns=['quality'], inplace=True)
    except Exception as e:
        print(f"An error occurred during quality metric calculations: {e}")

    # Calculate the highest and lowest quality scores for linear and logarithmic scales
    H_lin = strategy_df['q_lin'].max()
    H_log = strategy_df['q_log'].max()
    L_lin = strategy_df['q_lin'].min()
    L_log = strategy_df['q_log'].min()

    # Determine the length of each video based on the maximum segment index
    video_lengths = strategy_df.drop(columns=['tile', 'dl_time', 'dl_head', 'first_request_time',
                                              'first_request_head', 'last_request_time', 'last_request_head',
                                              'n_requests', 'score', 'size', 'cycle', 'ABR', 'pred', 'K',
                                              'B_min', 'B_max', 'user', 'tiling', 'segment_size', 'delta_dl', 'r',
                                              'EWMA_alpha', 'network_trace', 'q_lin', 'q_log']).groupby(['video'],
                                                                                                      as_index=False).max()
    video_lengths['length'] = video_lengths['segment'] + 1
    video_lengths = video_lengths.drop(columns=['segment'])
  
    # Prepare the visible download data
    strategy_visible_dl = strategy_df[(strategy_df['duration'] > 0) & (strategy_df['B_max'] == 10)].copy()
    strategy_visible_dl['dl_offset'] = strategy_visible_dl['last_request_head'] - (
            strategy_visible_dl['segment'] * strategy_visible_dl['segment_size'])
    strategy_visible_dl['discrete_dl_offset'] = np.round(strategy_visible_dl['dl_offset'] / 1000, decimals=0)
    strategy_visible_dl['discrete_dl_head'] = np.round(strategy_visible_dl['dl_head'] / 1000, decimals=0)

    strategy_visible_dl['weighted_q_lin'] = strategy_visible_dl['q_lin'] * strategy_visible_dl['duration']
    strategy_visible_dl['weighted_q_log'] = strategy_visible_dl['q_log'] * strategy_visible_dl['duration']

    # Aggregate data after making sure 'weighted_q_lin' and 'weighted_q_log' are still in the DataFrame
    strategy_visible_agg = strategy_visible_dl.drop(columns=['dl_time', 'dl_head', 'first_request_time',
                                                             'first_request_head', 'last_request_time',
                                                             'last_request_head', 'n_requests', 'score', 'size',
                                                             'cycle', 'dl_offset', 'discrete_dl_offset', 'discrete_dl_head',
                                                             'tile']).groupby(
        ['ABR', 'pred', 'K', 'B_min', 'B_max', 'video', 'user', 'tiling',
         'segment_size', 'delta_dl', 'r', 'EWMA_alpha', 'network_trace', 'segment'],
        as_index=False).sum()
   
    # Calculate normalized weighted quality scores
    strategy_visible_agg['weighted_q_lin'] = strategy_visible_agg['weighted_q_lin'] / strategy_visible_agg['duration']
    strategy_visible_agg['weighted_q_log'] = strategy_visible_agg['weighted_q_log'] / strategy_visible_agg['duration']

    # Drop the 'duration' column after its last use
    strategy_visible_agg = strategy_visible_agg.drop(columns=['duration'])

    # Optionally, if you need to drop weighted columns from the download data before saving:
    strategy_visible_dl = strategy_visible_dl.drop(columns=['weighted_q_lin', 'weighted_q_log'])

    # Save the visible download data after dropping weighted columns
    strategy_visible_dl.reset_index(drop=True).to_feather(f'{metrics_path}/visible_dl.feather')

    # Save the aggregated data after dropping 'duration' and recalculating weights
    strategy_visible_agg.reset_index(drop=True).to_feather(f'{metrics_path}/visible_agg.feather')

  # Compute and save the fairness indices for the aggregated data
    strategy_visible_agg_fairness = strategy_visible_agg.copy()
    strategy_visible_agg_fairness = strategy_visible_agg_fairness.groupby(['ABR', 'pred', 'K', 'B_min', 'B_max',
                                                                        'video', 'user', 'tiling',
                                                                        'segment_size', 'delta_dl', 'r',
                                                                        'EWMA_alpha', 'network_trace'],
                                                                        as_index=False).mean()
    strategy_visible_agg_fairness = strategy_visible_agg_fairness.groupby(['ABR', 'pred', 'K', 'B_min', 'B_max',
                                                                        'video', 'tiling', 'segment_size',
                                                                        'delta_dl', 'r', 'EWMA_alpha',
                                                                        'network_trace'],
                                                                        as_index=False).std()
    strategy_visible_agg_fairness['fairness_index_q_lin'] = 1 - (
            2 * strategy_visible_agg_fairness['weighted_q_lin'] / (H_lin - L_lin))
    strategy_visible_agg_fairness['fairness_index_q_log'] = 1 - (
            2 * strategy_visible_agg_fairness['weighted_q_log'] / (H_log - L_log))
    strategy_visible_agg_fairness.drop(columns=['segment', 'weighted_q_lin', 'weighted_q_log'], inplace=True)

    strategy_visible_agg_fairness.reset_index(drop=True).to_feather(
        f'{metrics_path}/visible_agg_fairness.feather')



    # Merge data for variance calculations
    strategy_visible_svar = pd.merge(strategy_visible_dl, strategy_visible_agg, 'left')

    # Calculate variances
    strategy_visible_svar['spatial_q_lin_variance'] = ((strategy_visible_svar['q_lin'] - strategy_visible_svar['weighted_q_lin']) ** 2) * strategy_visible_svar['duration']
    strategy_visible_svar['spatial_q_log_variance'] = ((strategy_visible_svar['q_log'] - strategy_visible_svar['weighted_q_log']) ** 2) * strategy_visible_svar['duration']

    # Drop unnecessary columns immediately after using them for variance calculations
    strategy_visible_svar.drop(columns=['dl_time', 'dl_head', 'first_request_time', 'first_request_head', 
                                        'last_request_time', 'last_request_head', 'n_requests', 'score', 
                                        'size', 'cycle', 'dl_offset', 'discrete_dl_offset', 'discrete_dl_head', 
                                        'tile', 'q_lin', 'q_log', 'weighted_q_lin', 'weighted_q_log'], 
                            inplace=True)

    # Perform grouping and further computations
    strategy_visible_svar = strategy_visible_svar.groupby(
        ['ABR', 'pred', 'K', 'B_min', 'B_max', 'video', 'user', 'tiling', 'segment_size', 
        'delta_dl', 'r', 'EWMA_alpha', 'network_trace', 'segment'], as_index=False).sum()

    # Normalize the variances by duration after grouping
    strategy_visible_svar['spatial_q_lin_variance'] /= strategy_visible_svar['duration']
    strategy_visible_svar['spatial_q_log_variance'] /= strategy_visible_svar['duration']

    # Drop duration after its last use
    strategy_visible_svar.drop(columns=['duration'], inplace=True)

    # Finally, save the DataFrame to a feather file
    strategy_visible_svar.reset_index(drop=True).to_feather(
        f'{metrics_path}/visible_svar.feather')



    # Start with the variance data
    strategy_visible_svar_fairness = strategy_visible_svar.copy()
    strategy_visible_svar_fairness = strategy_visible_svar.groupby(
        ['ABR', 'pred', 'K', 'B_min', 'B_max', 'video', 'user', 'tiling', 'segment_size', 
        'delta_dl', 'r', 'EWMA_alpha', 'network_trace'], as_index=False).mean()

    # Calculate standard deviation for fairness calculation
    strategy_visible_svar_fairness_std = strategy_visible_svar_fairness.groupby(
        ['ABR', 'pred', 'K', 'B_min', 'B_max', 'video', 'tiling', 'segment_size', 
        'delta_dl', 'r', 'EWMA_alpha', 'network_trace'], as_index=False).std()

    # Calculate fairness indices
    H_svar_lin = (((H_lin + L_lin) / 2) - L_lin) ** 2  # High threshold for spatial quality linear variance
    H_svar_log = (((H_log + L_log) / 2) - L_log) ** 2  # High threshold for spatial quality logarithmic variance
    L_svar_lin = 0  # Low threshold is 0 as variance cannot be negative
    L_svar_log = 0

    strategy_visible_svar_fairness['fairness_index_q_lin'] = 1 - (
        2 * strategy_visible_svar_fairness_std['spatial_q_lin_variance'] / (H_svar_lin - L_svar_lin))
    strategy_visible_svar_fairness['fairness_index_q_log'] = 1 - (
        2 * strategy_visible_svar_fairness_std['spatial_q_log_variance'] / (H_svar_log - L_svar_log))

    # Drop columns no longer needed after computing fairness indices
    strategy_visible_svar_fairness.drop(columns=['spatial_q_lin_variance', 'spatial_q_log_variance'], inplace=True)

    # Save the fairness data to a feather file
    strategy_visible_svar_fairness.reset_index(drop=True).to_feather(
        f'{metrics_path}/visible_svar_fairness.feather')



    # Start from the aggregated data
    strategy_visible_tvar = strategy_visible_agg.copy()
    strategy_visible_tvar['temporal_q_lin_variance'] = abs(strategy_visible_tvar['weighted_q_lin'].diff())
    strategy_visible_tvar['temporal_q_log_variance'] = abs(strategy_visible_tvar['weighted_q_log'].diff())

    # Handle variances for the first segment to avoid incorrect diffs
    strategy_visible_tvar['temporal_q_lin_variance'] = np.where(
        strategy_visible_tvar['segment'] == 0, np.nan, strategy_visible_tvar['temporal_q_lin_variance']
    )
    strategy_visible_tvar['temporal_q_log_variance'] = np.where(
        strategy_visible_tvar['segment'] == 0, np.nan, strategy_visible_tvar['temporal_q_log_variance']
    )

    # Clean up the DataFrame by dropping unnecessary columns and any NaN values
    strategy_visible_tvar.drop(columns=['segment', 'weighted_q_lin', 'weighted_q_log'], inplace=True)
    strategy_visible_tvar.dropna(inplace=True)

    # Calculate the mean of the temporal variances across all relevant groupings
    strategy_visible_tvar = strategy_visible_tvar.groupby(
        ['ABR', 'pred', 'K', 'B_min', 'B_max', 'video', 'user', 'tiling', 'segment_size', 
        'delta_dl', 'r', 'EWMA_alpha', 'network_trace'], as_index=False).mean()

    # Save the processed data to a feather file for further use
    strategy_visible_tvar.reset_index(drop=True).to_feather(
        f'{metrics_path}/visible_tvar.feather')



    strategy_visible_tvar_fairness = strategy_visible_tvar.copy()
    strategy_visible_tvar_fairness = strategy_visible_tvar_fairness.groupby(
        ['ABR', 'pred', 'K', 'B_min', 'B_max', 'video', 'tiling', 'segment_size', 'delta_dl', 'r', 'EWMA_alpha', 'network_trace'],
        as_index=False).std()

    # Calculate high and low thresholds for fairness calculations
    H_tvar_lin = H_lin - L_lin
    H_tvar_log = H_log - L_log
    L_tvar_lin = 0
    L_tvar_log = 0

    # Compute fairness indexes
    strategy_visible_tvar_fairness['fairness_index_q_lin'] = 1 - (
        2 * strategy_visible_tvar_fairness['temporal_q_lin_variance'] / (H_tvar_lin - L_tvar_lin))
    strategy_visible_tvar_fairness['fairness_index_q_log'] = 1 - (
        2 * strategy_visible_tvar_fairness['temporal_q_log_variance'] / (H_tvar_log - L_tvar_log))

    # Drop columns that are no longer needed after fairness index calculations
    strategy_visible_tvar_fairness.drop(columns=['temporal_q_lin_variance', 'temporal_q_log_variance'], inplace=True)

    # Save the fairness data to a feather file
    strategy_visible_tvar_fairness.reset_index(drop=True).to_feather(
        f'{metrics_path}/visible_tvar_fairness.feather')

    del strategy_visible_dl
    del strategy_visible_agg_fairness
    del strategy_visible_svar_fairness
    del strategy_visible_tvar_fairness


    stall_df = pd.read_feather(f'{CONCATENATED_DIR}/{video}/{B_min}/stall.feather')
    exp_stall_df = pd.read_feather(
        f'{CONCATENATED_DIR}/{video}/{1}/stall.feather')  # TODO: specify chosen B_min somewhere

    min_stall = exp_stall_df[
        (exp_stall_df['ABR'] == 'trivial') & (exp_stall_df['B_max'] == exp_stall_df['B_max'].max())].copy()
    min_stall = min_stall.drop(columns=['ABR',
                                        'pred',
                                        'K',
                                        'B_min',
                                        'B_max',
                                        'segment',
                                        'play_head',
                                        'downloaded_bits']).groupby(['video',
                                                                     'user',
                                                                     'tiling',
                                                                     'segment_size', 'delta_dl', 'r', 'EWMA_alpha',
                                                                     'network_trace'],
                                                                    as_index=False).sum().drop(
        columns=['user']).groupby(['video',
                                   'tiling',
                                   'segment_size', 'delta_dl', 'r', 'EWMA_alpha',
                                   'network_trace'],
                                  as_index=False).mean().rename(columns={
        'duration': 'min_stall'
    })

    max_stall = exp_stall_df[
        (exp_stall_df['ABR'] == 'max_stall') & (exp_stall_df['B_max'] == exp_stall_df['B_max'].min())].copy()
    max_stall = max_stall.drop(columns=['ABR',
                                        'pred',
                                        'K',
                                        'B_min',
                                        'B_max',
                                        'segment',
                                        'play_head',
                                        'downloaded_bits']).groupby(['video',
                                                                     'user',
                                                                     'tiling',
                                                                     'segment_size', 'delta_dl', 'r', 'EWMA_alpha',
                                                                     'network_trace'],
                                                                    as_index=False).sum().drop(
        columns=['user']).groupby(['video',
                                   'tiling',
                                   'segment_size', 'delta_dl', 'r', 'EWMA_alpha',
                                   'network_trace'],
                                  as_index=False).mean().rename(columns={
        'duration': 'max_stall'
    })

    # Adapt the stall DataFrame to include data based on the ABR strategy specified
    strategy_stall = stall_df[(stall_df['ABR'] == abr_strategy)].copy()

    # Generate a temporary DataFrame from downloaded data that includes only unique entries for the current strategy
    tmp_df = downloaded_df[downloaded_df['ABR'] == abr_strategy][
        ['ABR', 'pred', 'K', 'B_min', 'B_max', 'video', 'user', 'tiling', 'segment_size', 'network_trace', 'delta_dl',
        'r', 'EWMA_alpha', 'segment']].drop_duplicates()

    # Merge the temporary DataFrame with the stall DataFrame to ensure all necessary data is present
    strategy_stall = pd.merge(tmp_df, strategy_stall, 'left').fillna(0)

    # Handle cases where play_head might be zero by adjusting it based on segment size
    bad_play_head = strategy_stall[strategy_stall['play_head'] == 0]
    strategy_stall.loc[strategy_stall['play_head'] == 0, 'play_head'] = bad_play_head['segment'] * bad_play_head[
        'segment_size'] + bad_play_head['segment_size'] // 2

    # Clean up temporary data structures
    del tmp_df, bad_play_head

    # Save the processed stall data for the current strategy to a feather file
    strategy_stall.reset_index(drop=True).to_feather(f'{metrics_path}/stall.feather')


    # Aggregate the stall data for the current ABR strategy, dropping unnecessary columns
    strategy_stall_agg = strategy_stall.drop(columns=['segment', 'play_head', 'downloaded_bits']).groupby(
        ['ABR', 'pred', 'K', 'B_min', 'B_max', 'video', 'user', 'tiling', 'segment_size', 'delta_dl', 'r', 'EWMA_alpha', 'network_trace'],
        as_index=False).sum()

    # Save the aggregated stall data to a feather file, naming it according to the current ABR strategy
    strategy_stall_agg.reset_index(drop=True).to_feather(
        f'{metrics_path}/stall_agg.feather')


    # Calculate the standard deviation for the aggregated stall data as a measure of fairness
    strategy_stall_agg_fairness = strategy_stall_agg.groupby(
        ['ABR', 'pred', 'K', 'B_min', 'B_max', 'video', 'tiling', 'segment_size', 'delta_dl', 'r', 'EWMA_alpha', 'network_trace'],
        as_index=False).std(numeric_only=True)

    # Merge with minimum and maximum stall data to calculate a fairness index
    strategy_stall_agg_fairness = pd.merge(strategy_stall_agg_fairness, min_stall,'left').fillna(0.0)
    strategy_stall_agg_fairness = pd.merge(strategy_stall_agg_fairness, max_stall, 'left')

    # Compute the fairness index based on stall duration
    strategy_stall_agg_fairness['fairness_index'] = 1 - (2 * strategy_stall_agg_fairness['duration'] / (
        strategy_stall_agg_fairness['max_stall'] - strategy_stall_agg_fairness['min_stall']))

    # Clean up the DataFrame by dropping now unnecessary columns
    strategy_stall_agg_fairness.drop(columns=['duration', 'min_stall', 'max_stall'], inplace=True)

    # Save the fairness data to a feather file, named according to the current ABR strategy
    strategy_stall_agg_fairness.reset_index(drop=True).to_feather(
        f'{metrics_path}/stall_agg_fairness.feather')

    # Clean up variables to free up memory
    del min_stall, max_stall, stall_df, strategy_stall, strategy_stall_agg_fairness


    # Aggregate and merge data to prepare for QoE calculations
    strategy_qoe = pd.merge(
        pd.merge(
            strategy_visible_agg.drop(columns=['segment']).groupby(
                ['ABR', 'pred', 'K', 'B_min', 'B_max', 'video', 'user', 'tiling', 'segment_size', 'delta_dl', 'r', 'EWMA_alpha', 'network_trace'],
                as_index=False).mean(),
            strategy_stall_agg.rename(columns={'duration': 'stall'}),
            'inner'
        ),
        video_lengths,
        'left'
    )

    # Convert stall duration from milliseconds to seconds for QoE calculations
    strategy_qoe['stall'] /= 1000

    # Integrate variance data from spatial and temporal quality variance calculations
    strategy_qoe = pd.merge(
        strategy_qoe,
        strategy_visible_svar.drop(columns=['segment']).groupby(
            ['ABR', 'pred', 'K', 'B_min', 'B_max', 'video', 'user', 'tiling', 'segment_size', 'delta_dl', 'r', 'EWMA_alpha', 'network_trace'],
            as_index=False).mean(),
        'inner'
    )

    # Further merge with temporal variance data
    strategy_qoe = pd.merge(
        strategy_qoe,
        strategy_visible_tvar,
        'inner'
    )

    # Calculate the linear and logarithmic QoE
    strategy_qoe['qoe_lin'] = (
        (strategy_qoe['weighted_q_lin'] * strategy_qoe['length']) / ((strategy_qoe['stall'] + strategy_qoe['length']) * H_lin)
    ) * (1 - (strategy_qoe['spatial_q_lin_variance'] / (2 * H_svar_lin))) * (1 - (strategy_qoe['temporal_q_lin_variance'] / (2 * H_tvar_lin)))

    strategy_qoe['qoe_log'] = (
        (strategy_qoe['weighted_q_log'] * strategy_qoe['length']) / ((strategy_qoe['stall'] + strategy_qoe['length']) * H_log)
    ) * (1 - (strategy_qoe['spatial_q_log_variance'] / (2 * H_svar_log))) * (1 - (strategy_qoe['temporal_q_log_variance'] / (2 * H_tvar_log)))

    # Save the QoE data to a feather file, named according to the current ABR strategy
    strategy_qoe.reset_index(drop=True).to_feather(
        f'{metrics_path}/qoe.feather')


    # Calculate the standard deviation of QoE metrics for fairness evaluation
    strategy_qoe_fairness = strategy_qoe.drop(
        columns=['weighted_q_lin', 'weighted_q_log', 'stall', 'length', 'spatial_q_lin_variance', 'spatial_q_log_variance', 'temporal_q_lin_variance', 'temporal_q_log_variance']
    ).groupby(
        ['ABR', 'pred', 'K', 'B_min', 'B_max', 'video', 'tiling', 'segment_size', 'delta_dl', 'r', 'EWMA_alpha', 'network_trace'],
        as_index=False
    ).std()

    # Compute fairness indices for linear and logarithmic QoE
    strategy_qoe_fairness['fairness_index_lin'] = 1 - (2 * strategy_qoe_fairness['qoe_lin'])
    strategy_qoe_fairness['fairness_index_log'] = 1 - (2 * strategy_qoe_fairness['qoe_log'])

    # Remove the no longer needed QoE columns after calculating fairness indices
    strategy_qoe_fairness.drop(columns=['qoe_lin', 'qoe_log'], inplace=True)

    # Save the QoE fairness data to a feather file, named according to the current ABR strategy
    strategy_qoe_fairness.reset_index(drop=True).to_feather(
        f'{metrics_path}/qoe_fairness.feather')

    # Clean up loaded DataFrames to free memory
    del video_lengths, strategy_visible_agg, strategy_visible_svar, strategy_visible_tvar, strategy_stall_agg, strategy_qoe, strategy_qoe_fairness


    bw_util_df = pd.read_feather(f'{CONCATENATED_DIR}/{video}/{B_min}/bw_util.feather')

    strategy_bw = bw_util_df[(bw_util_df['ABR'] == abr_strategy) & (bw_util_df['B_max'] == 10)].copy()
    strategy_bw = strategy_bw.drop(
        columns=['cycle', 'cycle_start', 'est_latency', 'est_bw', 'cycle_end', 'est_bw_usage', 'true_bw_usage'])
    strategy_bw['adjusted_cycle_duration'] = strategy_bw['cycle_duration'] - strategy_bw['true_latency']
    strategy_bw['available_bits'] = strategy_bw['true_bw'] * strategy_bw['adjusted_cycle_duration']
    strategy_bw_agg = strategy_bw.drop(columns=['true_latency',
                                                'true_bw',
                                                'cycle_duration',
                                                'adjusted_cycle_duration']).groupby(
        ['ABR', 'pred', 'K', 'B_min', 'B_max', 'video',
         'user', 'tiling', 'segment_size', 'delta_dl', 'r', 'EWMA_alpha', 'network_trace'],
        as_index=False).sum()
    strategy_bw_agg['bw_usage'] = strategy_bw_agg['bits_dl'] / strategy_bw_agg['available_bits']

    strategy_bw_agg.reset_index(drop=True).to_feather(f'{metrics_path}/bw_agg.feather')

    strategy_bw_efficiency_agg = strategy_df[
        (strategy_df['ABR'] == abr_strategy) & (strategy_df['B_max'] == 10)].copy()
    strategy_efficiency_agg = strategy_bw_efficiency_agg.drop(
        columns=['segment', 'tile', 'q_lin', 'q_log', 'dl_time',
                 'dl_head', 'first_request_time', 'first_request_head',
                 'last_request_time', 'last_request_head', 'n_requests',
                 'score', 'cycle'])
    strategy_bw_efficiency_agg['hit'] = strategy_bw_efficiency_agg['duration'] > 0
    strategy_bw_efficiency_agg = strategy_bw_efficiency_agg.groupby(
        ['ABR', 'pred', 'K', 'B_min', 'B_max', 'video', 'user',
         'tiling', 'segment_size', 'delta_dl', 'r', 'EWMA_alpha', 'network_trace', 'hit'],
        as_index=False).sum()
    strategy_bw_efficiency_agg = strategy_bw_efficiency_agg.drop(columns=['duration'])
    strategy_bw_efficiency_agg = \
        strategy_bw_efficiency_agg.set_index(['ABR', 'pred', 'K', 'B_min', 'B_max', 'video', 'user',
                                              'tiling', 'segment_size', 'delta_dl', 'r', 'EWMA_alpha', 'network_trace',
                                              'hit'])[
            'size'].unstack().reset_index().rename(columns={False: 'miss', True: 'hit'})
    
    if 'hit' in strategy_bw_efficiency_agg.columns and 'miss' in strategy_bw_efficiency_agg.columns:
        strategy_bw_efficiency_agg['hit_rate'] = strategy_bw_efficiency_agg['hit'] / (
            strategy_bw_efficiency_agg['hit'] + strategy_bw_efficiency_agg['miss'])
    else:
        print("Required columns are missing.")

    strategy_bw_efficiency_agg.reset_index(drop=True).to_feather(
        f'{metrics_path}/bw_efficiency_agg.feather')

    del downloaded_df
    del strategy_df
    del bw_util_df
    del strategy_bw
    del strategy_bw_agg
    del strategy_bw_efficiency_agg



def process_metrics_helper(args):
    video, B_min, abr_strategy = args  # This unpacks the tuple correctly for three arguments
    try:
        logging.info(f"Starting to process metrics for video: {video}, B_min: {B_min}, abr_strategy: {abr_strategy}")
        result = process_metrics(video, B_min, abr_strategy)
        logging.info(f"Successfully processed metrics for video: {video}, B_min: {B_min}, abr_strategy: {abr_strategy}")
        return result
    except Exception as e:
        logging.error(f"Error processing metrics for video: {video}, B_min: {B_min}, abr_strategy: {abr_strategy}: {e}")
        return None

def process_all_metrics(n_cpus=max(1, os.cpu_count() - 2), timeout=300):
    abr_strategies = ['enhanced_prefetch']  # List all the strategies you want to process

    tasks = []
    for video in sorted(os.listdir(f'{CONCATENATED_DIR}')):
        for B_min in sorted(os.listdir(f'{CONCATENATED_DIR}/{video}')):
            for abr_strategy in abr_strategies:
                tasks.append((video, B_min, abr_strategy))

    logging.info(f"Starting to process metrics with {len(tasks)} tasks")

    with ThreadPoolExecutor(max_workers=n_cpus) as executor:
        future_to_task = {executor.submit(process_metrics_helper, task): task for task in tasks}
        with tqdm(total=len(tasks)) as pbar:
            for future in as_completed(future_to_task, timeout=timeout):
                task = future_to_task[future]
                try:
                    result = future.result()
                    # You can handle the result here if needed
                except Exception as e:
                    logging.error(f"Task {task} generated an exception: {e}")
                pbar.update()

def concatenate_metrics(item):
    metric_df, video_B_min_strategy = item
    dataframes = []
    for video, B_min, strategy in video_B_min_strategy:
        file_path = f'{METRICS_DIR}/{video}/{B_min}/{strategy}/{metric_df}.feather'
        if os.path.exists(file_path):
            df = pd.read_feather(file_path)
            dataframes.append(df)
        else:
            print(f"File not found: {file_path}")
    if dataframes:
        concatenated_df = pd.concat(dataframes).reset_index(drop=True)
        os.makedirs(f'{CONCATENATED_METRICS_DIR}/{metric_df}', exist_ok=True)
        concatenated_df.to_feather(f'{CONCATENATED_METRICS_DIR}/{metric_df}/{video}_{B_min}_{strategy}.feather')
    else:
        print(f"No dataframes to concatenate for {metric_df}")



def concatenate_all_metrics(n_cpus=max(1, os.cpu_count() - 2)):
    os.makedirs(CONCATENATED_METRICS_DIR, exist_ok=True)
    metrics_dict = {}
    for video in sorted(os.listdir(METRICS_DIR)):
        for B_min in sorted(os.listdir(f'{METRICS_DIR}/{video}')):
            for strategy in sorted(os.listdir(f'{METRICS_DIR}/{video}/{B_min}')):
                for metric_file in sorted(os.listdir(f'{METRICS_DIR}/{video}/{B_min}/{strategy}')):
                    metric_df = metric_file.replace('.feather', '')
                    key = (metric_df, video, B_min, strategy)
                    metrics_dict.setdefault(metric_df, []).append((video, B_min, strategy))

    with multiprocessing.Pool(n_cpus) as pool:
        with tqdm(total=len(metrics_dict)) as pbar:
            for _ in pool.imap_unordered(concatenate_metrics, metrics_dict.items()):
                pbar.update()
