# SMART360 Simulator
This repository contains code for the SMART360 simulator.
Here, you can:
- scale the network traces to match the video bitrates for the ABR to be useful; 
- run 360° streaming simulations with the SMART360 simulator with various configurations and settings;
- visualize the simulation results to compare ABRs and head motion prediction algorithms.

## Citing this work
```
@inbook{10.1145/3587819.3592547,
author = {Guimard, Quentin and Sassatelli, Lucile},
title = {SMART360: Simulating Motion Prediction and Adaptive BitRate STrategies for 360° Video Streaming},
year = {2023},
isbn = {9798400701481},
publisher = {Association for Computing Machinery},
address = {New York, NY, USA},
url = {https://doi.org/10.1145/3587819.3592547},
abstract = {Adaptive bitrate (ABR) algorithms are used in streaming media to adjust video or audio quality based on the viewer's network conditions to provide a smooth playback experience. With the rise of virtual reality (VR) headsets, 360° video streaming is growing rapidly and requires efficient ABR strategies to also adapt the video quality to the user's head position. However, research in this field is often difficult to compare due to a lack of reproducible simulations. To address this problem, we provide SMART360, a 360° streaming simulation environment to compare motion prediction and adaptive bitrates strategies. We provide sample inputs and baseline algorithms along with the simulator, as well as examples of results and visualizations that can be obtained with SMART360. The code and data are made publicly available.},
booktitle = {Proceedings of the 14th Conference on ACM Multimedia Systems},
pages = {384–390},
numpages = {7}
}
```

<img src="https://www.acm.org/binaries/content/gallery/acm/publications/artifact-review-v1_1-badges/artifacts_evaluated_functional_v1_1.png" alt="ACM badge: Artifacts Evaluated & Reusable / v1.1" width="100">

## Authors
- [Quentin Guimard](mailto:quentin.guimard@univ-cotedazur.fr) - Université Côte d'Azur, CNRS, I3S
- Lucile Sassatelli - Université Côte d'Azur, CNRS, I3S, Institut Universitaire de France

## Getting started
We recommend using Docker. We provide an [image](https://hub.docker.com/repository/docker/qguim/smart360-sim_env/general) with the required software and libraries installed.
Please refer to the [official documentation](https://docs.docker.com/engine/install/) to install Docker on your machine if it is not already installed.

Follow these instructions to set up the SMART360 simulator with Docker:
```shell
git clone https://gitlab.com/SMART360/SMART360-simulator.git /path/to/SMART360-simulator
chmod 777 /path/to/SMART360-simulator  # give file permissions to docker in SMART360 if the id does not match by default (docker uid/gid: 1000)
docker pull qguim/smart360-sim_env:latest
docker run -d -it --rm -v /path/to/SMART360-simulator:/SMART360-simulator -p 8888:8888 --name SMART360-simulator qguim/smart360-sim_env:latest
docker attach SMART360-simulator
cd /SMART360-simulator
python ./run_many.py -p ./run_parameters.json  # run simulations and parse logs, configuration and settings can be changed by editing the JSON parameter file
cd ./notebooks
jupyter notebook --ip 0.0.0.0  # start jupyter to access and run notebooks
```

## Sample output files
We provide the results of some simulations on synthetic data. The configuration and settings are the same as the ones in `run_parameters.json`.
[Click here](https://unice-my.sharepoint.com/:f:/g/personal/quentin_guimard_unice_fr/EpTkPRTtFrpOvv-UcHib-iYBrDH3Q-lOU2ZNlRseffoc7w?e=Jselxi) to download this data.

## Running the code
### Network traces scaling
The notebook for scaling the network traces can be found in the `notebooks` directory. Please refer to the [Getting started](#getting-started) section to start Jupyter.

By default, Jupyter will be accessible at localhost:8888 on your machine.
This is because the Jupyter server is running on port 8888 inside the container (by default) and we are binding port 8888 of the container to port 8888 the host machine.

You can change that port when creating the Docker container by changing the port with the option `-p <HOST_PORT>:<DOCKER_PORT>` of the `docker run` command. 

### Simulator
You can run a single simulation by executing the `run_single.py` Python file. Type `python run_single.py -h` from the root directory for a comprehensive list of the available parameters.
Running simulations will produce JSON log files in the specified destination folder.

If you want to run many simulations, you can use `run_many.py`. The parameters for these simulations can be edited in `run_parameters.json`.

### Log parsing
You can (re)parse the simulation logs from a single run by executing the `parse_single` Python file. Type `python parse_single.py -h` from the root directory for more information about the required parameters.

(**Unnecessary** if simulations were run with `run_many.py`). If you want to (re)parse many log files, you can use `parse_many.py`. The parameters for these simulations can be edited in `run_parameters.json` (the parameters will target the corresponding log files).

### Visualizations
The notebook for visualizing the simulation results can be found in the `notebooks` directory. Please refer to the [Getting started](#getting-started) section to start Jupyter.

> &#x26a0;&#xfe0f; **The visualization can be memory intensive**: You can reduce the number of processes during post-processing or run the notebook on a smaller subset of output data.

By default, Jupyter will be accessible at localhost:8888 on your machine.
This is because the Jupyter server is running on port 8888 inside the container (by default) and we are binding port 8888 of the container to port 8888 the host machine.

You can change that port when creating the Docker container by changing the port with the option `-p <HOST_PORT>:<DOCKER_PORT>` of the `docker run` command. 
