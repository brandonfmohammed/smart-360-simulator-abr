# SMART360 Simulator
# Copyright (C) 2023  Quentin Guimard
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import json

import pandas as pd


def parse_logs(log_file_path):
    with open(log_file_path, 'r') as f:
        logs = json.load(f)

    assert logs[0]['category'] == 'config'
    config = logs[0]['value']
    # B_max = int(config['B_max'])

    with open(config['manifest'], 'r') as f:
        manifest = json.load(f)

    # tiles_x = manifest['tiles_x']
    # tiles_y = manifest['tiles_y']
    # n_tiles = tiles_x * tiles_y
    segment_duration = manifest['segment_duration_ms']

    schedule_records = []
    dl_records = []
    visible_records = []
    play_records = []
    pred_records = []
    stall_records = []
    network_estimation_records = []
    network_actual_records = []
    startup_records = []

    for record in logs:
        cat = record['category']
        calc_seg = int(record['play_head'] / segment_duration)

        if cat == 'dl_schedule':
            for s, t, q in record['value']:
                schedule_records.append({'segment': s, 'tile': t, 'quality': q, 'request_time': record['wall_time'], 'request_head': record['play_head']})
        elif cat == 'dl_success':
            for s, t, q in record['value']:
                dl_records.append({'segment': s, 'tile': t, 'quality': q, 'dl_time': record['wall_time'], 'dl_head': record['play_head']})
        elif cat == 'visible_tiles':
            for tile in record['value']:
                visible_records.append({'segment': calc_seg, 'tile': tile, 'start_time': record['play_head']})
        elif cat == 'playing_video':
            play_records.append({'segment': calc_seg, 'start_time': record['play_head'], 'duration': record['value']})
        elif cat == 'tile_prediction':
            seg, scores = record['value']
            for tile, score in enumerate(scores):
                pred_records.append({'segment': seg, 'tile': tile, 'score': score, 'pred_time': record['wall_time']})
        elif cat == 'stall':
            time, bits = record['value']
            stall_records.append({'segment': calc_seg, 'play_head': record['play_head'], 'duration': time, 'downloaded_bits': bits})
        elif cat == 'network_estimation':
            latency, bw, cycle = record['value']
            network_estimation_records.append({'cycle': cycle, 'cycle_start': record['wall_time'], 'est_latency': latency, 'est_bw': bw})
        elif cat == 'network_actual':
            latency, bw, cycle = record['value']
            network_actual_records.append({'cycle': cycle, 'cycle_end': record['wall_time'], 'true_latency': latency, 'true_bw': bw})
        elif cat == 'startup':
            time, bits = record['value']
            startup_records.append({'duration': time, 'downloaded_bits': bits})

    df_schedule = pd.DataFrame.from_records(schedule_records)
    df_download = pd.DataFrame.from_records(dl_records)
    df_visible = pd.DataFrame.from_records(visible_records)
    df_play = pd.DataFrame.from_records(play_records)
    df_pred = pd.DataFrame.from_records(pred_records)
    df_stall = pd.DataFrame.from_records(stall_records)
    df_estimated_network = pd.DataFrame.from_records(network_estimation_records)
    df_actual_network = pd.DataFrame.from_records(network_actual_records)
    df_startup = pd.DataFrame.from_records(startup_records)

    group_requests_by_stq = df_schedule.groupby(['segment', 'tile', 'quality'], sort=False)
    df_schedule = df_schedule.drop(columns=['request_time', 'request_head']).drop_duplicates()
    stq_min = group_requests_by_stq.min()
    df_schedule['first_request_time'] = stq_min['request_time'].values
    df_schedule['first_request_head'] = stq_min['request_head'].values
    stq_max = group_requests_by_stq.max()
    df_schedule['last_request_time'] = stq_max['request_time'].values
    df_schedule['last_request_head'] = stq_max['request_head'].values
    df_schedule['n_requests'] = group_requests_by_stq.count()['request_time'].values
    df_download = pd.merge(df_download, df_schedule, 'left', ['segment', 'tile', 'quality'])
    if len(df_pred) > 0:
        df_pred = pd.merge(df_pred, df_download, 'left', ['segment', 'tile'])
        df_pred = df_pred[df_pred['first_request_time'] - df_pred['pred_time'] == 0].drop(columns=['pred_time', 'quality', 'dl_time', 'dl_head', 'first_request_time', 'last_request_time', 'first_request_head', 'last_request_head', 'n_requests'])
        df_download = pd.merge(df_download, df_pred, 'left', ['segment', 'tile'])
    else:
        df_download['score'] = 0
    df_visible = pd.merge(df_visible, df_play, 'left', ['segment', 'start_time']).drop(columns=['start_time']).groupby(['segment', 'tile'], as_index=False, sort=False).sum()
    df_download = pd.merge(df_download, df_visible, 'left', ['segment', 'tile']).fillna(0.0)
    df_download['size'] = df_download.apply(lambda x: manifest['segment_sizes_bits'][int(x.segment)][int(x.tile)][int(x.quality)], axis=1)
    df_network = pd.merge(df_estimated_network, df_actual_network, 'inner', ['cycle'])
    df_download['cycle'] = (pd.cut(df_download['dl_time'], df_network['cycle_start'], labels=False) + 1).fillna(0).astype(int)
    bits_by_cycle = df_download[['cycle', 'size']].groupby(['cycle'], as_index=False, sort=False).sum()
    df_network = pd.merge(df_network, bits_by_cycle, 'left', ['cycle']).fillna(0.0).rename(columns={'size': 'bits_dl'})
    df_network['cycle_duration'] = df_network['cycle_end'] - df_network['cycle_start']
    df_network['est_bw_usage'] = df_network['bits_dl'] / (df_network['est_bw'] * (df_network['cycle_duration'] - df_network['est_latency']))
    df_network['true_bw_usage'] = df_network['bits_dl'] / (df_network['true_bw'] * (df_network['cycle_duration'] - df_network['true_latency']))

    output_file_prefix = log_file_path[:-5]

    df_download.to_feather(f'{output_file_prefix}-downloaded.feather')
    if len(df_stall) != 0:
        df_stall.to_feather(f'{output_file_prefix}-stall.feather')
    df_network.to_feather(f'{output_file_prefix}-bw_util.feather')
    df_startup.to_feather(f'{output_file_prefix}-startup.feather')
