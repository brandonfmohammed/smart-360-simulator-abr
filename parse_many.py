# SMART360 Simulator
# Copyright (C) 2023  Quentin Guimard
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# SMART360 Simulator
# Copyright (C) 2023  Quentin Guimard
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import argparse
import itertools
import multiprocessing
import json
import os

from log_parsing import parse_logs


ABR_DICT = {
    'trivial': 'TrivialABR',
    'baseline': 'BaselineABR',
    'max_stall': 'MaxStallABR',
    'enhanced_prefetch': 'EnhancedPrefetchABR'
}
PRED_DICT = {
    'no_pred': 'NoPredictor',
    'static_pred': 'StaticPredictor',
    'dvms': 'DVMSPredictor'
}

parser = argparse.ArgumentParser(
    prog='SMART360',
    description='SMART360: Simulating Motion prediction and Adaptive bitRate sTrategies for 360° video streaming',
)

parser.add_argument('-p', '--parameters',
                    dest='parameters',
                    default='./run_parameters.json',
                    help='Path to simulation parameters.')
args = parser.parse_args()

parameters = json.load(open(args.parameters, 'r'))

parameters_combinations = itertools.product(parameters['ABRS'],
                                            parameters['PREDICTORS'],
                                            parameters['KS'],
                                            parameters['B_MINS'])
CHOSEN_B_MIN = parameters['B_MINS'][0]  # Choose one value of B_min (doesn't matter which) for ABRs other than BaselineABR
parameters_combinations_clean = []
for ABR, predictor, K, B_min in parameters_combinations:
    if ABR in ['trivial', 'max_stall'] and predictor != 'no_pred':  # Avoid useless simulations since TrivialABR and MaxStallABR do not use predictions
        continue
    if K != 1 and predictor[:4] != 'dvms':  # Avoid useless simulations since only DVMS uses the K parameter
        continue
    if ABR in ['trivial', 'max_stall'] and B_min != CHOSEN_B_MIN:
        continue
    parameters_combinations_clean.append((ABR, predictor, K, B_min))
parameters_combinations = parameters_combinations_clean
parameters_combinations_clean = []

video_users_combinations = []
if parameters['VIDEOS'] == 'all':
    videos = sorted(os.listdir(f"{parameters['VIDEOS_PATH']}"))
else:
    videos = parameters['VIDEOS']
for video in videos:
    if parameters['USERS'] == 'all':  # TODO: allow custom user choice, currently only supports all available users for a given video
        video_users_combinations.extend([(video, user_file) for user_file in sorted(os.listdir(f"{parameters['USERS_PATH']}/{video}"))])

parameters_combinations = itertools.product(parameters_combinations,
                                            video_users_combinations)
if parameters['NETWORKS'] == 'all':
    networks = sorted(os.listdir(f"{parameters['NETWORKS_PATH']}"))
else:
    networks = parameters['NETWORKS']
parameters_combinations = itertools.product(parameters_combinations,
                                            parameters['B_MAXS'],
                                            networks,
                                            parameters['TILINGS'],
                                            parameters['SEGMENTS'],
                                            parameters['DELTA_DLS'],
                                            parameters['RS'],
                                            parameters['EWMA_ALPHAS'])

SMART360_PATH = parameters['SMART360_PATH'] + '/'
# SMART360_PATH = '/home/quentin/Documents/smart360/'
OUTPUT_PATH = SMART360_PATH + parameters['OUTPUT_PATH']


def reparse_logs(parameters):
    ((ABR, predictor, K, B_min), (video, user)), B_max, network, tiling, segment, delta_dl, r, EWMA_alpha = parameters
    log_file = f'{OUTPUT_PATH}/{ABR}-ABR/{predictor}/K={K}/B_min={B_min}/B_max={B_max}/video-{video}/user-{user[:-5]}/{tiling}-tiling/{segment}-segment/network-{network[:-5]}/delta_dl={delta_dl}/r={r}/EWMA_alpha={EWMA_alpha}/logs.json'
    if os.path.exists(log_file):
        print(f'Parsing {log_file}')
        try:
            parse_logs(log_file)
        except json.decoder.JSONDecodeError:
            print(f'Error parsing {log_file}')
    else:
        print(f'{log_file} does not exist!')


with multiprocessing.Pool(max(1, os.cpu_count() - 2)) as pool:
    pool.map(reparse_logs, parameters_combinations)
